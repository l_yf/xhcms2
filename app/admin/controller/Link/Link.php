<?php 
/**
 *友情链接
*/

namespace app\admin\controller\Link;

use xhadmin\service\admin\Link\LinkService;
use xhadmin\db\Link\Link as LinkDb;
use app\admin\controller\Admin;

class Link extends Admin {


	/*友情链接*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['a.title'] = $this->request->param('title', '', 'strip_tags,trim');
			$where['a.status'] = $this->request->param('status', '', 'strip_tags,trim');
			$where['a.cata_id'] = $this->request->param('cata_id', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			$field = 'a.*,b.*';
			try{
				$list = LinkDb::relateQuery($field,'cata_id',$relate_table='link_catagory',$relate_field='cata_id',formatWhere($where),$limit,$orderby);
				$res['count'] = LinkDb::relateQueryCount($field,'cata_id',$relate_table='link_catagory',$relate_field='cata_id',formatWhere($where));
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			LinkDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('add');
		}else{
			$postField = 'title,url,status,sortid,logo,cata_id,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				LinkService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			if(!$id) $this->error('参数错误');
			$this->view->assign('info',checkData(LinkDb::getInfo($id)));
			return $this->display('update');
		}else{
			$postField = 'id,title,url,status,logo,cata_id,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				LinkService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['id'] = explode(',',$idx);
			LinkService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看数据*/
	function view(){
		$id = $this->request->get('id','','intval');
		if(!$id) $this->error('参数错误');
		try{
			$this->view->assign('info',checkData(LinkDb::getInfo($id)));
			return $this->display('view');
		} catch (\Exception $e){
			$this->error($e->getMessage());
		}
	}



}

