<?php 
/**
 *用户管理
*/

namespace app\admin\controller;

use xhadmin\service\admin\UserService;
use xhadmin\db\User as UserDb;

class User extends Admin {


	/*用户管理  <font color="red">初始密码都为000000</font>*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['a.user'] = $this->request->param('user', '', 'strip_tags,trim');
			$where['a.group_id'] = $this->request->param('group_id', '', 'strip_tags,trim');
			$where['a.type'] = $this->request->param('type', '', 'strip_tags,trim');
			$where['a.status'] = $this->request->param('status', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			$field = 'a.*,b.name as group_name';
			try{
				$list = UserDb::relateQuery($field,'group_id',$relate_table='group',$relate_field='group_id',formatWhere($where),$limit,$orderby);
				$res['count'] = UserDb::relateQueryCount($field,'group_id',$relate_table='group',$relate_field='group_id',formatWhere($where));
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*添加账户*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('add');
		}else{
			$postField = 'name,user,pwd,group_id,type,note,status,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				UserService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改账户*/
	function update(){
		if (!$this->request->isPost()){
			$user_id = $this->request->get('user_id','','intval');
			if(!$user_id) $this->error('参数错误');
			$this->view->assign('info',checkData(UserDb::getInfo($user_id)));
			return $this->display('update');
		}else{
			$postField = 'user_id,name,user,group_id,type,note,status,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				UserService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*修改密码*/
	function updatePassword(){
		if (!$this->request->isPost()){
			$info['user_id'] = $this->request->get('user_id','','intval');
			$this->view->assign('info',$info);
			return $this->display('updatePassword');
		}else{
			$data = $this->request->post();
			try {
				$data['pwd'] = md5($data['pwd'].config('my.password_secrect'));
				UserDb::edit($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*删除数据*/
	function delete(){
		$idx =  $this->request->post('user_ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['user_id'] = explode(',',$idx);
			UserService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}



}

